# Payment Tracker #

This is a payment tracker application. Please read readme before running of application. Application requires java 8.

### How to run? ###

* For building of application use maven and run command: *mvn clean install*
* Go to the *target* folder.
* Run jar file e.g: 
    * Without currency/exchange rate files *java -jar tracker-1.0-SNAPSHOT-jar-with-dependencies*
    * With currency file *java -jar  tracker-1.0-SNAPSHOT-jar-with-dependencies currency-file-path*
    * With currency and exchange rate file *java -jar  tracker-1.0-SNAPSHOT-jar-with-dependencies currency-file-path exchange-rate-file-path*
### Control of running application ###

* Quit application - write *quit*
* Add currency - Write currency with amount in right format e.g: *USD 300* and press enter

### Application behavior ###
 In case that user add input in the wrong format, some message can be shown. Application is not stopped. User can add input again.
 
* Message examples: 
    * Illegal format - in case that string representation of input is invalid
    * Number format exception - in case that amount or exchange rate can not be parsed.
    * Exchange rate has to be positive or zero - if exchange rate < 0
    * Wrong currency amount/exchange rate format.
    
 * Error message related with files
    * Problem with reading of file - This message is shown when I/O exception occur. Application is still alive user can use it.

### Examples folder ###

Contains 2 example files.
* currencies.txt - Shows how file with currency should looks like
* rates.txt - Shows how configuration file with the exchange rate should looks like.
