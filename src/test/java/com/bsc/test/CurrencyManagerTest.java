package com.bsc.test;

import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;

import com.bsc.tracker.currency.Currency;
import com.bsc.tracker.currency.CurrencyManager;

/**
 * <p>
 * Title: {@link CurrencyManagerTest}
 * </p>
 * <p>
 * Description: Currency manager tests
 * </p>
 *
 * @author Richard Lipka
 * @date 10/29/2019 9:38 AM
 */
public class CurrencyManagerTest {

	/**
	 * Refference to currency manager
	 */
	private CurrencyManager currencyManager;

	@Before
	public void initialization() {
		currencyManager = new CurrencyManager();
	}

	@Test
	public void testCurrencyRelatedOperations() {
		Currency currencyUsd500 = currencyManager.addCurrencyAsString("USD 500");
		Assert.assertNotNull(currencyUsd500);
		Currency currencyMinusUsd150 = currencyManager.addCurrencyAsString("USD -150");
		Assert.assertNotNull(currencyMinusUsd150);
		currencyManager.addCurrencyAsString("EUR 400");
		Assert.assertEquals(2, currencyManager.getCurrencyList().size());
		currencyManager.addCurrency(new Currency("EUR", -400));
		Assert.assertEquals(1, currencyManager.getCurrenciesInPrintableFormat().size());
	}

	@Test
	public void testExchangeRateOperations() {
		currencyManager.addExchangeRate("EUR 1.2");
		currencyManager.addExchangeRate("RMD 0.14");
		currencyManager.addExchangeRate("RMD" ,0.11);
		Assert.assertEquals(2, currencyManager.getExchangeRate().size());
	}

	@Test
	public void testWrongCurrencyFormats() {
		Assert.assertNull(currencyManager.addCurrencyAsString("usd 100"));
		Assert.assertNull(currencyManager.addCurrencyAsString("USD100"));
		Assert.assertNull(currencyManager.addCurrencyAsString("USD"));
		Assert.assertNull(currencyManager.addCurrencyAsString("USDD 0"));
		Assert.assertNull(currencyManager.addCurrencyAsString("U 10"));
	}

	@Test
	public void testWrongExchangeRateFormats() {
		currencyManager.addExchangeRate("EURO 1.2");
		currencyManager.addExchangeRate("EUR -1.2");
		currencyManager.addExchangeRate("E 1.2");
		currencyManager.addExchangeRate("EUR1.2");
		Assert.assertEquals(0, currencyManager.getExchangeRate().size());
	}
}
