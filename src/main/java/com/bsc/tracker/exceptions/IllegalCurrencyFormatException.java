package com.bsc.tracker.exceptions;

/**
 * <p>
 * Title: {@link IllegalCurrencyFormatException}
 * </p>
 * <p>
 * Description: Illegal currency format exception.
 * </p>
 *
 * @author Richard Lipka
 * @date 10/28/2019 6:09 PM
 */
public class IllegalCurrencyFormatException extends Exception {

	/**
	 * Constructs a new exception with the specified detail message.  The
	 * cause is not initialized, and may subsequently be initialized by
	 * a call to {@link #initCause}.
	 *
	 * @param message the detail message. The detail message is saved for
	 *                later retrieval by the {@link #getMessage()} method.
	 */
	public IllegalCurrencyFormatException(String message) {
		super(message);
	}
}
