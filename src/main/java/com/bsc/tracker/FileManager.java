package com.bsc.tracker;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Collections;
import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import com.bsc.tracker.currency.CurrencyManager;

/**
 * <p>
 * Title: {@link FileManager}
 * </p>
 * <p>
 * Description: File manager which is responsible for reading of files and filling of data in to currency manager memory.
 * </p>
 *
 * @author Richard Lipka
 * @date 10/28/2019 11:06 AM
 */
public class FileManager {

	/**
	 * Currency manager
	 */
	private final CurrencyManager currencyManager;

	public FileManager(CurrencyManager aCurrencyManager) {
		currencyManager = aCurrencyManager;
	}

	/**
	 * Read configuration file and add values in to currencyManager.
	 * Configuration file contains exchange rates.
	 * Each exchange rate has to be in separate line.
	 *
	 * @param fileName Configuration file name
	 */
	public void fillExchangeRates(String fileName) {
			getFileStream(fileName).forEach(currencyManager::addExchangeRate);
	}

	/**
	 * Read currency file and add values in to currencyManager.
	 * Files contains currencies in format <b>CURRENCY AMOUNT</b>.
	 * Each currency has to be in separate line.
	 *
	 * @param fileName Configuration file name
	 */
	public void fillCurrencies(String fileName) {
			getFileStream(fileName).forEach(currencyManager::addCurrencyAsString);
	}

	/**
	 * Get list of file lines
	 *
	 * @param fileName Path to file with file name
	 * @return List contains lines from file.
	 */
	private List<String> getFileStream(String fileName) {
		try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
			return stream.collect(Collectors.toList());
		} catch (IOException e) {
			System.out.println("Problem with reading of file." + e.getMessage());
		}
		return Collections.emptyList();
	}

}
