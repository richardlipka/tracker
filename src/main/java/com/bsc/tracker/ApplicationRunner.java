package com.bsc.tracker;

import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

import com.bsc.tracker.currency.CurrencyManager;

/**
 * <p>
 * Title: {@link ApplicationRunner}
 * </p>
 * <p>
 * Description: Application runner is main class which manage all basic
 * functionality like initialization of managers, Configure of tasks, etc.
 * </p>
 *
 * @author Richard Lipka
 * @date 10/29/2019 9:58 AM
 */
public class ApplicationRunner {

	/**
	 * Initial delay constant for printing output task
	 */
	private static final int INITIAL_DELAY = 1;

	/**
	 * Period constant for printing output task
	 */
	private static final int PERIOD = 1;

	/**
	 * Thread pool scheduler
	 */
	private final ScheduledThreadPoolExecutor threadPoolExecutor = new ScheduledThreadPoolExecutor(1);

	/**
	 * Currency manager
	 */
	private CurrencyManager currencyManager;

	/**
	 * Initialization of currency manager. Currency manager can contain
	 *
	 * @param fileNames Array which contains file names. Can be null.
	 */
	public void init(String[] fileNames) {
		currencyManager = new CurrencyManager();

		// Initialize file manager
		FileManager fileManager = new FileManager(currencyManager);

		if (fileNames.length > 1) {
			// Contains currency file and configuration file
			fileManager.fillCurrencies(fileNames[0]);
			fileManager.fillExchangeRates(fileNames[1]);
		}
		else if (fileNames.length == 1) {
			// Contains only currency file
			fileManager.fillCurrencies(fileNames[0]);
		}
		else {
			System.out.println("No initialization files. Continue...");
		}

	}

	/**
	 * Start application. Initialization of scheduler which is responsible for printing task.
	 * Initialization of scanner which is responsible for reading of inputs.
	 */
	public void start() {
		System.out.println("Payment tracker - Started");
		System.out.println("If you want to finish program write 'quit'");

		// Schedule task which is responsible for printing of output in console
		threadPoolExecutor.scheduleAtFixedRate(new OutputGeneratorTask(), INITIAL_DELAY, PERIOD, TimeUnit.MINUTES);

		// Start command line manager which block this thread
		CommandLineManager commandLineManager = new CommandLineManager(currencyManager);
		commandLineManager.start();

		threadPoolExecutor.shutdownNow();
		System.out.println("Payment tracker - Finished");
	}

	/**
	 * Purpose of this task is printing of output in console.
	 */
	private final class OutputGeneratorTask implements Runnable {

		/**
		 * Print amount of all currencies in to console.
		 */
		public void run() {
			if (currencyManager != null) {
				System.out.println("---------- CURRENCIES ----------");
				currencyManager.getCurrenciesInPrintableFormat().forEach(System.out::println);
			}
		}
	}
}
