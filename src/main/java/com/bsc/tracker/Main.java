package com.bsc.tracker;

/**
 * <p>
 * Title: {@link Main}
 * </p>
 * <p>
 * Description: Main class which is responsible for starting of program.
 * </p>
 *
 * @author Richard Lipka
 * @date 10/28/2019 10:41 AM
 */
public class Main {

	/**
	 * Application runner
	 */
	private static ApplicationRunner applicationRunner;

	/**
	 * Main method which is called when program is started.
	 * @param args arguments
	 */
	public static void main(String[] args) {
		applicationRunner = new ApplicationRunner();
		applicationRunner.init(args);
		applicationRunner.start();
	}
}
