package com.bsc.tracker.currency;

/**
 * <p>
 * Title: {@link Currency}
 * </p>
 * <p>
 * Description: Currency object which hold all data related with currency.
 * </p>
 *
 * @author Richard Lipka
 * @date 10/28/2019 10:43 AM
 */
public class Currency {

	/**
	 * Currency name
	 */
	private String name;

	/**
	 * Currency amount
	 */
	private int amount;

	/**
	 * Default constructor
	 */
	public Currency() {
		super();
	}

	/**
	 * Constructor
	 *
	 * @param aName currency name
	 * @param aAmount amount
	 */
	public Currency(String aName, int aAmount) {
		name = aName;
		amount = aAmount;
	}

	/**
	 * @return the name
	 */
	public String getName() {
		return name;
	}

	/**
	 * @param aName the name
	 */
	public void setName(String aName) {
		name = aName;
	}

	/**
	 * @return the amount
	 */
	public int getAmount() {
		return amount;
	}

	/**
	 * @param aAmount the amount
	 */
	public void setAmount(int aAmount) {
		amount = aAmount;
	}
}
