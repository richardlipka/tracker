package com.bsc.tracker.currency;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentMap;

import org.apache.commons.lang3.StringUtils;

import com.bsc.tracker.exceptions.IllegalCurrencyFormatException;

/**
 * <p>
 * Title: {@link CurrencyManager}
 * </p>
 * <p>
 * Description: Manager store all currencies and is responsible for thread safe operations.
 * </p>
 *
 * @author Richard Lipka
 * @date 10/28/2019 11:09 AM
 */
public final class CurrencyManager {

	/**
	 * Currency output formats
	 */
	private static final String CURRENCY_FORMAT_NO_EXCHANGE_RATE = "%s %d";
	private static final String CURRENCY_FORMAT_WITH_EXCHANGE_RATE = "%s %d (USD %.4f)";

	/**
	 * Lock which is responsible for synchronization.
	 */
	private static Object lock = new Object();

	/**
	 * List of currencies
	 */
	private List<Currency> currencyList = new ArrayList<>();

	/**
	 * Hold exchange rate compared to USD. For example 1 RMB = 0.14 USD -> <RMB, 0.14>
	 */
	private ConcurrentMap<String, Double> exchangeRate = new ConcurrentHashMap<>();


	/////////////////////////////////////////////////////////////////////////////
	// Public interfaces
	/////////////////////////////////////////////////////////////////////////////

	/**
	 * Add currency string in to memory. Parse currency in string format and add it in currency memory.
	 * Thread safe operation.
	 *
	 * @param currencyString Currency string in format <b>CURRENCY AMOUNT</b>
	 * @return Currency object if creating of currency object was successful, otherwise {@code null}
	 */
	public Currency addCurrencyAsString(String currencyString) {

		try {
			Currency currency = parseCurrency(currencyString);
			addCurrency(currency);
			return currency;
		}
		catch (IllegalCurrencyFormatException e) {
			System.out.println(e.getMessage());
		}
		catch (NumberFormatException e) {
			System.out.println("Wrong currency amount format. Amount has to be integer value.");
		}
		return null;
	}

	/**
	 * Add currency. Thread safe operation.
	 *
	 * @param currency {@link Currency}
	 */
	public void addCurrency(Currency currency) {
		synchronized (lock) {

			// Check if currencyList contains currency.
			Currency currencyToUpdate = currencyList
					.stream()
					.filter(currencyInList -> currencyInList.getName().equalsIgnoreCase(currency.getName()))
					.findFirst()
					.orElse(null);

			if (currencyToUpdate != null) {
				// Update currency amount
				currencyToUpdate.setAmount(currencyToUpdate.getAmount() + currency.getAmount());
			}
			else {
				// Add new currency
				currencyList.add(currency);
			}

		}
	}

	/**
	 * Add exchange rate in string format in to tj memory.
	 *
	 * @param exchangeRateString Exchange rate in string format.
	 */
	public void addExchangeRate(String exchangeRateString) {
		try {
			parseExchangeRate(exchangeRateString);
		}
		catch (IllegalCurrencyFormatException e) {
			System.out.println(e.getMessage());
		}
		catch (NumberFormatException e) {
			System.out.println("Wrong exchange rate format. Rate has to be decimal value.");
		}
	}

	/**
	 * Add exchange rate for currency.
	 *
	 * @param currency Currency shortcut [EUR, USD]
	 * @param rate     Rate compared to USD
	 */
	public void addExchangeRate(String currency, double rate) {
		exchangeRate.put(currency, rate);
	}

	/**
	 * Get list of currencies. Thread safe operation.
	 *
	 * @return List of {@link Currency}
	 */
	public List<Currency> getCurrencyList() {
		synchronized (lock) {
			return new ArrayList<>(currencyList);
		}
	}

	/**
	 * @return the exchangeRate
	 */
	public ConcurrentMap<String, Double> getExchangeRate() {
		return exchangeRate;
	}

	/**
	 * Get list of currencies in string format. Only currencies with amount <> 0 are included.
	 *
	 * @return List of {@link Currency}
	 */
	public List<String> getCurrenciesInPrintableFormat() {
		List<String> currencyListToPrint = new ArrayList<>();

		synchronized (lock) {
			// Fill currency list which will be printed in output.
			currencyList.stream().filter(currency -> currency.getAmount() != 0).forEach(currency -> {

				// FIND exchange rate for currency
				Double rate = exchangeRate.get(currency.getName());
				if (rate == null) {
					currencyListToPrint.add(String.format(CURRENCY_FORMAT_NO_EXCHANGE_RATE, currency.getName(), currency.getAmount()));
				}
				else {
					int amount = currency.getAmount();
					Double exchRate = amount * rate;
					currencyListToPrint.add(String.format(CURRENCY_FORMAT_WITH_EXCHANGE_RATE, currency.getName(), amount, exchRate));
				}

			});
		}

		return currencyListToPrint;
	}

	/////////////////////////////////////////////////////////////////////////////
	// Internal routines
	/////////////////////////////////////////////////////////////////////////////

	/**
	 * Internal routine. Parse String in currency object.
	 *
	 * @param currencyString currency string
	 * @return Currency object if parsing of currency string was successful, otherwise false.
	 * @throws NumberFormatException          If parsing of amount was not successful
	 * @throws IllegalCurrencyFormatException If currency string has wrong format
	 */
	private Currency parseCurrency(String currencyString) throws IllegalCurrencyFormatException {
		Currency currency = new Currency();
		String[] currencyArray = parseLine(currencyString);
		currency.setName(currencyArray[0]);
		currency.setAmount(Integer.parseInt(currencyArray[1]));
		return currency;
	}

	/**
	 * Parse exchange rate string.
	 *
	 * @param exchangeRateString - exchange rate string to be parsed.
	 * @throws NumberFormatException          If parsing of rate was not successful
	 * @throws IllegalCurrencyFormatException If currency string has wrong format
	 */
	private void parseExchangeRate(String exchangeRateString) throws IllegalCurrencyFormatException {
		String[] exchangeRateData = parseLine(exchangeRateString);
		double rate = Double.parseDouble(exchangeRateData[1]);
		if (rate < 0) {
			throw new IllegalCurrencyFormatException("Exchange rate has to be positive or zero.");
		}
		exchangeRate.put(exchangeRateData[0], rate);
	}

	/**
	 * Parse line which has to consist of 2 String. First string has to be currency and the second String has to be number.
	 *
	 * @param line String line to be parsed
	 * @return Array of parsed string.
	 * @throws IllegalCurrencyFormatException If line has wrong format.
	 */
	private String[] parseLine(String line) throws IllegalCurrencyFormatException {
		String[] parsedData = line.split("\\s+");
		if (parsedData.length != 2 || parsedData[0].toCharArray().length != 3 || !StringUtils.isAllUpperCase(parsedData[0])) {
			throw new IllegalCurrencyFormatException("Illegal format.");
		}
		return parsedData;
	}

}
