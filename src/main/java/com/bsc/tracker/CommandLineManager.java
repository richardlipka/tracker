package com.bsc.tracker;

import java.util.Scanner;

import com.bsc.tracker.currency.CurrencyManager;

/**
 * <p>
 * Title: {@link CommandLineManager}
 * </p>
 * <p>
 * Description: Command line manager is responsible for handling of inputs from console.
 * </p>
 *
 * @author Richard Lipka
 * @date 10/29/2019 11:01 AM
 */
public class CommandLineManager {

	/**
	 * Reference to currency manager
	 */
	private final CurrencyManager currencyManager;

	/**
	 * Constructor
	 *
	 * @param aCurrencyManager {@link CurrencyManager}
	 */
	public CommandLineManager(CurrencyManager aCurrencyManager) {
		currencyManager = aCurrencyManager;
	}

	/**
	 * Start command line runner which is responsible for reading input from console.
	 */
	public void start() {
		Scanner scanner = new Scanner(System.in);
		boolean isRunning = true;
		while (isRunning && scanner.hasNext()) {

			String line = scanner.nextLine();

			if (line.equalsIgnoreCase("quit")) {
				isRunning = false;
				System.out.println("Quit program");
			}
			else {
				currencyManager.addCurrencyAsString(line);
			}
		}

	}
}
